Parallel (MPI) solver for non-periodic tridiagonal systems of equations: Contains the functions for solving non-periodic tridiagonal systems of equations; written in C, and uses the MPI library. An optional ScaLAPACK wrapper is also available. Detailed documentation: http://tridiaglu.github.io/index.html

